package com.epitech.RSS.application.Interface;

import com.epitech.RSS.application.user.User;

import java.util.List;

public interface IUserService {
    User getById(Integer id);
    boolean addUser(User user);
    void updateUser(User user);
    void deleteUser(Integer id);
    User getIdByName(String name);
}
