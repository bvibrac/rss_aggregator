package com.epitech.RSS.application.service;

import com.epitech.RSS.application.Interface.IUserService;
import com.epitech.RSS.application.user.User;
import com.epitech.RSS.application.user.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@Service
public class UserService implements IUserService {
    @Autowired
    private UserRepository userRepository;

    @Override
    public synchronized boolean addUser(User user){
        List<User> list = userRepository.findUserByName(user.getName());
        if (list.size() > 0)
            return false;
        else {
            userRepository.save(user);
            return true;
        }
    }

    @Override
    public void updateUser(User user) {
        userRepository.save(user);
    }

    @Override
    public void deleteUser(Integer id) {
        userRepository.delete(getById(id));
    }

    @Override
    public User getById(Integer id) {
        User user = userRepository.findOne(id);
        return user;
    }

    @Override
    public User getIdByName(String name) {
        List<User> user = userRepository.findUserByName(name);
        return user.get(0);
    }
}
