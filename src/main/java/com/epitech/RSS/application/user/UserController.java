package com.epitech.RSS.application.user;

import com.epitech.RSS.application.Interface.IUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping(path = "/user")
public class UserController {
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private IUserService userService;

    @GetMapping(path = "/add")
    public @ResponseBody
    ResponseEntity<User> addUser(@RequestParam String name, @RequestParam String email, @RequestParam String password) {
        User newU = new User();
        newU.setName(name);
        newU.setEmail(email);
        newU.setPassword(password);
        boolean flag = userService.addUser(newU);
        if (!flag)
            return new ResponseEntity<User>(HttpStatus.CONFLICT);
        userRepository.save(newU);
        return new ResponseEntity<User>(newU, HttpStatus.CREATED);
    }

    @GetMapping(path = "/getId")
    public ResponseEntity<Integer> getId(@RequestParam String name){
        User users = userService.getIdByName(name);
        if (users == null){
            return new ResponseEntity<Integer>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<Integer>(users.getId(), HttpStatus.OK);
    }

    @GetMapping(path = "/get")
    public @ResponseBody
    Iterable<User> getAllUser() {
        return userRepository.findAll();
    }

    @GetMapping(path = "/get/{id}")
    public @ResponseBody
    ResponseEntity<User> getById(@PathVariable Integer id) {
        User user = userService.getById(id);
        if (user == null) {
            return new ResponseEntity<User>(user, HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<User>(user, HttpStatus.OK);
    }

    @PutMapping(path = "/update")
    public ResponseEntity<User> updateArticle(@RequestBody User user) {
        if (user == null){
            return new ResponseEntity<User>(HttpStatus.NOT_FOUND);
        }
        System.out.print(user);
        userService.updateUser(user);
        return new ResponseEntity<User>(user, HttpStatus.OK);
    }

    @DeleteMapping("/delete/{id}")
    public ResponseEntity<Void> deleteArticle(@PathVariable("id") Integer id) {
        User user = userService.getById(id);
        if (user == null){
            return new ResponseEntity<Void>(HttpStatus.NOT_FOUND);
        }
        userService.deleteUser(id);
        return new ResponseEntity<Void>(HttpStatus.OK);
    }
}

