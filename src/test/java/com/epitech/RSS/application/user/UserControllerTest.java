package com.epitech.RSS.application.user;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.web.client.RestTemplate;


public class UserControllerTest {
    public void updateArticleDemo() {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        RestTemplate restTemplate = new RestTemplate();
        String url = "http://localhost:8080/user/update";
        User user = new User();
        user.setId(6);
        user.setName("Update:Java Concurrency");
        user.setEmail("Java");
        user.setPassword("pouet");
        HttpEntity<User> requestEntity = new HttpEntity<User>(user, headers);
        restTemplate.put(url, requestEntity);
    }

    public void deleteArticleDemo() {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        RestTemplate restTemplate = new RestTemplate();
        String url = "http://localhost:8080/user/delete/{id}";
        HttpEntity<User> requestEntity = new HttpEntity<User>(headers);
        restTemplate.exchange(url, HttpMethod.DELETE, requestEntity, Void.class, 5);
    }

    public static void main(String args[]) {
        UserControllerTest util = new UserControllerTest();
        //util.getArticleByIdDemo();
        //util.addArticleDemo();
        //util.updateArticleDemo();
//        util.deleteArticleDemo();
        util.updateArticleDemo();
    }
}
